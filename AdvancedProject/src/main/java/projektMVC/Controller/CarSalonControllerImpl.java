package projektMVC.Controller;


import projektMVC.Model.*;

public class CarSalonControllerImpl implements CarSalonMVC.Controller {
    private Printer printer;
    private ScannerWrapper scannerWrapper;
    private Car car;
    private Wallet wallet;

    public Wallet getWallet() {
        return wallet;
    }

    public Car getCar() {
        return car;
    }

    public CarSalonControllerImpl(Printer printer, ScannerWrapper scannerWrapper) {
        this.printer = printer;
        this.scannerWrapper = scannerWrapper;
    }

    @Override
    public void showGreetings() {
        printer.showGreetings();
    }

    @Override
    public void start() {
        car = new Car();
    }

    @Override
    public void chooseColor() {
        int option = scannerWrapper.nextInt();

        switch (option) {
            case 1:
                if (Color.RED.price > wallet.getCash()) {
                } else {
                    chooseModel();
                }
                break;
            case 2:
                if (Color.BLUE.price > wallet.getCash()) {
                } else {
                    chooseModel();
                }
                break;
            case 0:
                return;
        }
    }

    public void chooseModel() {
        int option = scannerWrapper.nextInt();

        switch (option) {
            case 1:
                if (Model.AUDI.price > wallet.getCash()) {

                } else {
                    chooseFuel();
                }
                break;
            case 2:
                if (Model.SEAT.price > wallet.getCash()) {

                } else {
                    chooseFuel();
                }
                break;
            case 0:
                chooseColor();
                break;
        }
    }

    public void chooseFuel() {
        int option = scannerWrapper.nextInt();

        switch (option) {
            case 1:
                if (Fuel.PETROL.price > wallet.getCash()) {

                } else {
                    chooseType();
                }
                break;
            case 2:
                if (Fuel.DIESEL.price > wallet.getCash()) {

                } else {
                    chooseType();
                }
                break;
            case 0:
                chooseModel();
                break;
        }
    }

    public void chooseType() {
        int option = scannerWrapper.nextInt();

        switch (option) {
            case 1:
                if (TypeCar.SEDAN.price > wallet.getCash()) {

                } else {
                    return;
                }
                break;
            case 2:
                if(TypeCar.COMBI.price>wallet.getCash()){

                }else {
                    return;
                }
                break;
            case 0:
                return;
        }
    }
    public Car finish(){
        return car;
    }
}