package projektMVC.Controller;

public class Wallet {

    int cash=10000;

    public Wallet(int cash) {
        this.cash = cash;
    }

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }
}
