package projektMVC.Model;

public interface CarSalonMVC {
    interface Controller {
        void showGreetings();
        void start();
        void chooseColor();
        void chooseModel();
        void chooseFuel();
        void chooseType();
        Car finish();
    }
    interface View {
    }
}
