package projektMVC.Model;

public enum  Color {
    RED(1000,"Czerwony"),
    BLUE(1000,"Niebieski");

    public int price;
    public String name;


    Color(int price, String name) {
        this.price = price;
        this.name = name;
    }
}
