package projektMVC.Model;

public enum  TypeCar {
    SEDAN("Sedan",100),

    COMBI("Kombi",300);

    public String typeCar;
    public int price;


    TypeCar(String typeCar, int price) {
        this.typeCar = typeCar;
        this.price = price;


    }

}

