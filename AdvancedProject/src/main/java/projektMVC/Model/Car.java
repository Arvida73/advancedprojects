package projektMVC.Model;

public class Car {
    private boolean color;
    private Model model;
    private Fuel fuel;
    private TypeCar typeCar;

    @Override
    public String toString() {
        return "Car{" +
                "color=" + color +
                ", model=" + model +
                ", fuel=" + fuel +
                ", typeCar=" + typeCar +
                '}';
    }

    public TypeCar getTypeCar() {
        return typeCar;
    }

    public void setTypeCar(TypeCar typeCar) {
        this.typeCar = typeCar;
    }

    public boolean getColor() {
        return color;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Fuel getFuel() {
        return fuel;
    }

    public void setFuel(Fuel fuel) {
        this.fuel = fuel;
    }

    public boolean setColor(boolean color) {
        this.color = color;
        return color;
    }
}
