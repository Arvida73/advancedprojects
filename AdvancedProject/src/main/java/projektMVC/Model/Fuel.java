package projektMVC.Model;

public enum  Fuel {
    DIESEL(2000,"Dizel"),
    PETROL(1000,"Benzyna");
    public int price;
    public String name;

    Fuel(int price, String name) {
        this.price = price;
        this.name = name;
    }
}
