package projektMVC.View;

import projektMVC.Controller.CarSalonControllerImpl;
import projektMVC.Controller.PrinterImpl;
import projektMVC.Controller.ScannerWrapperImpl;
import projektMVC.Model.CarSalonMVC;

public class Main implements CarSalonMVC.View {
    public static void main(String[] args) {
        CarSalonMVC.Controller controller = new CarSalonControllerImpl(new PrinterImpl(), new ScannerWrapperImpl());

        controller.start();
        controller.chooseColor();
        controller.chooseModel();
        controller.chooseFuel();
        controller.chooseType();
        controller.finish();
        System.out.println(controller.finish());
        System.out.println("wwwww");
    }
}

