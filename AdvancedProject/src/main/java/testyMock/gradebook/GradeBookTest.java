package testyMock.gradebook;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import java.util.ArrayList;


class GradeBookTest {
    private GradeBook gradeBook = new GradeBook();

    @BeforeEach
    public void initialize() {
        gradeBook.addSubject(new Subject("Math"));
        gradeBook.addSubject(new Subject("Biology"));
        gradeBook.addSubject(new Subject("Geography"));
    }

    @Test
    public void get_subject() {
        Assertions.assertEquals(new ArrayList<>(), gradeBook.getSubject("Math").getGrades());
    }

//    @Test
//    public void set_grade() {
//        gradeBook.getSubject("Math").addGrade(new BigDecimal("4.5"));
//        gradeBook.getSubject("Math").addGrade(new BigDecimal("4.0"));
//        Assertions.assertEquals(new BigDecimal("4.5"), gradeBook.getSubject("Math").getGrades().get(0));
//    }
//
//    @Test
//    public void subject_average() {
//        gradeBook.getSubject("Math").addGrade(new BigDecimal("4.5"));
//        gradeBook.getSubject("Math").addGrade(new BigDecimal("4.0"));
//        gradeBook.getSubject("Math").addGrade(new BigDecimal("5.0"));
//        Assertions.assertEquals(new BigDecimal("4.5"), gradeBook.getSubject("Math").average());
//    }
//
//    @Test
//    public void grade_book_average() {
//        gradeBook.getSubject("Math").addGrade(new BigDecimal("4.5"));
//        gradeBook.getSubject("Math").addGrade(new BigDecimal("4.0"));
//        gradeBook.getSubject("Math").addGrade(new BigDecimal("5.0"));
//        gradeBook.getSubject("Biology").addGrade(new BigDecimal("3.0"));
//        gradeBook.getSubject("Geography").addGrade(new BigDecimal("4.5"));
//        Assertions.assertEquals(new BigDecimal("4.0"), gradeBook.average());
//    }
}
