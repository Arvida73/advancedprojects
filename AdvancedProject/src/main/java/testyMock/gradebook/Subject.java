package testyMock.gradebook;

import java.util.ArrayList;
import java.util.List;

public class Subject {

    private final String name;
  private List<Double> grades;

    public Subject(String name) {
        this.name = name;
       this.grades = new ArrayList<>();
    }


    public String getName() {
        return name;
    }

    public List<Double> getGrades() {
        return grades;
    }
//
//    public void addGrade(BigDecimal grade) {
//        grades.add(grade);
//    }
//
//    public BigDecimal average() {
//        return grades.stream()
//                .reduce(BigDecimal.ZERO, BigDecimal::add)
//                .divide(new BigDecimal(grades.size()));
//    }
//}
}
