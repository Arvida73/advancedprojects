package testyMock.gradebook;

import java.util.ArrayList;
import java.util.List;

public class GradeBook {
    private List<Subject> subjects = new ArrayList<>();

    public void addSubject(Subject subject) {
        subjects.add(subject);
    }

    public Subject getSubject(String name) {
        for (Subject subject : subjects) {
            if (subject.getName().equals(name)) {
                return subject;
            }
        }
        return null;
    }

//    public BigDecimal average() {
//        return subjects.stream()
//                .map(Subject::average)
//                .reduce(BigDecimal.ZERO, BigDecimal::add)
//                .divide(new BigDecimal(subjects.size()));
//    }
}