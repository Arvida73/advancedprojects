//package testyMock.personapi;
//
//import com.sun.tools.javac.util.List;
//import org.junit.jupiter.api.Test;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//class PersonAPI2Test {
//    private List<Person> personList = List.of(new Person(5), new Person(10), new Person(15));
//    private PersonAPI2 personAPI2 = mock(PersonAPI2.class);
//    @Test
//    public void testPeopleAPI() {
//        when(personAPI2.getPeople()).thenReturn(personList);
//        assertEquals(personList, personAPI2.getPeople());
//    }
//}