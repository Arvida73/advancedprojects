package testyMock.methoddsource;

import org.junit.jupiter.params.ParameterizedTest;

import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;


import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {
    @ParameterizedTest
    @MethodSource("MethodSource")
    public void parameterizedTest(int age,boolean isAdult) {
        assertEquals(isAdult,new Person(age).isAdult());

    }

    private static Stream<Arguments> MethodSource() {
        return Stream.of(Arguments.of(18, true), Arguments.of(2, false), Arguments.of(17, false));
    }


}