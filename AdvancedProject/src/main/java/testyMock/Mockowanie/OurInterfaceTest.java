package testyMock.Mockowanie;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.Test;

public class OurInterfaceTest {
    OurInterface ourInterface = Mockito.mock(OurInterface.class);

    @Test
    public void testInterface(){
        Mockito.when(ourInterface.fool1(Mockito.anyInt())).thenReturn("TEST");
        Mockito.when(ourInterface.fool1(100)).thenReturn("TEST");
        Mockito.when(ourInterface.fool1(101)).thenReturn("TEST2");

        Assert.assertEquals(ourInterface.fool1(100),"test");
        Assert.assertEquals(ourInterface.fool1(101),"test2");
    }
}