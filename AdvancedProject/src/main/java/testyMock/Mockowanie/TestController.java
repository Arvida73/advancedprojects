package testyMock.Mockowanie;

public class TestController {
    private OurInterface ourInterface;

    public TestController(OurInterface ourInterface) {
        this.ourInterface = ourInterface;
    }

    public String returnOurInterfaceValue(int value) {
        if (value == 10) {
            return ourInterface.fool1(value); //"123"
        } else if (value == 11) {
            return ourInterface.fool1(value);//"test
        } else if (value == 15) {
            return ourInterface.fool1(value);//"ala ma kota
        }
        return ourInterface.fool1(value);// "!"
    }
}
