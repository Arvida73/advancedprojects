package testyMock.Mockowanie;


import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.testng.Assert;

public class TestControllerTest {
    OurInterface ourInterfaceMock = Mockito.mock(OurInterface.class);
    TestController tested = new TestController(ourInterfaceMock);


    @Test
    public void testMethod1(){
        Mockito.when(ourInterfaceMock.fool1(10)).thenReturn("123");
        Assert.assertEquals(tested.returnOurInterfaceValue(10),"123");
    } @Test
    public void testMethod2(){
        Mockito.when(ourInterfaceMock.fool1(11)).thenReturn("test");
        Assert.assertEquals(tested.returnOurInterfaceValue(11),"test");
    }@Test
    public void testMethod3(){
        Mockito.when(ourInterfaceMock.fool1(15)).thenReturn("ala ma kota");
        Assert.assertEquals(tested.returnOurInterfaceValue(15),"ala ma kota");
    }@Test
    public void testMethod4(){
        Mockito.when(ourInterfaceMock.fool1(Mockito.anyInt())).thenReturn("!");
        Assert.assertEquals(tested.returnOurInterfaceValue(1000),"!");
    }
}









