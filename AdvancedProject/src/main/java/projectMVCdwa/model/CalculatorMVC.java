package model;

public interface CalculatorMVC {

    interface Controller{
        void add(int valueFirst, int valueSecond);
        void substract(int valueFirst,int valueSecond);

    }
    interface View {
    }
}
